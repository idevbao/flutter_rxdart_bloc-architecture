class User {
  final String id;
  final String email;
  final String numberPhone;
  final String sex;

  User.fromJson(Map json)
      : id = json['id'],
        email = json['email'],
        numberPhone = json['numberPhone'],
        sex = json['sex'];
}
