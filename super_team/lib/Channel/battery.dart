import 'dart:async';

import 'package:flutter/services.dart';

class Battery {
  final platform = const MethodChannel('dev.ibao.superteam/battery');
  Future<String> getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }
    return batteryLevel;
  }
}

