import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:superteam/BLoC/location_query_bloc.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'package:superteam/Repo/Location.dart';
import 'package:superteam/BLoC/location_bloc.dart';
import 'dart:io';

class LocationScreen extends StatelessWidget {
  final bool isFullScreenDialog;

  const LocationScreen({Key key, this.isFullScreenDialog = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final bloc = LocationQueryBLoC();
    // TODO: implement build
    return
      BlocProvider<LocationQueryBLoC>(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Where do you want to eat?'),
          actions: <Widget>[_actionUploadFile(bloc), _actionCheckBattery(bloc)],
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 10.0),
              child: TextField(
                onChanged: bloc.changeQuery,
                keyboardType: TextInputType.phone,
              ),
            ),
            TextField(onChanged: bloc.email),
            TextField(onChanged: bloc.pass),
            RaisedButton(onPressed: bloc.login),
            _buildEmailStream(bloc),
            Expanded(
              child: _buildResults(bloc),
            ),
            Expanded(
              child: _buildAppLibraryDirectory(bloc),
            ),
            _buildGetBatteryStream(bloc),
            Expanded(
              child: _buildAlertResultsStream(bloc),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildResults(LocationQueryBLoC bloc) {
    return StreamBuilder<List<Location>>(
      stream: bloc.locationStream$,
      builder: (context, snapshot) {
        final results = snapshot.data;
        if (results == null) {
          return Center(child: Text('Enter a location'));
        }
        if (results.isEmpty) {
          return Center(child: Text('No Results'));
        }
        return _buildSearchResults(results);
      },
    );
  }

  Widget _buildSearchResults(List<Location> results) {
    return ListView.separated(
      itemCount: results.length,
      separatorBuilder: (BuildContext context, int index) => Divider(),
      itemBuilder: (context, index) {
        final location = results[index];
        return ListTile(
          title: Text(location.title),
          onTap: () {
            final locationBloc = BlocProvider.of<LocationBLoC>(
                context); // call len Main_screen de lay object locationBloc
            locationBloc.selectLocation(location);
            if (isFullScreenDialog) {
              Navigator.of(context).pop();
            }
          },
        );
      },
    );
  }

  Widget _buildAppLibraryDirectory(LocationQueryBLoC bloc) {
    return StreamBuilder<Directory>(
      stream: bloc.getLibraryDirectory$,
      builder: (context, snapshot) {
        final results = snapshot.data;
        if (results == null) {
          return Center(child: Text('path unavailable'));
        }
        if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (snapshot.hasData) {
          return Center(child: Text('path: ${snapshot.data.path}'));
        }
        return Center();
      },
    );
  }

  Widget _buildGetBatteryStream(LocationQueryBLoC bloc) {
    return StreamBuilder<String>(
      stream: bloc.getBatteryStream$,
      builder: (context, snapshot) {
        return Center(child: Text('%: ${snapshot.data}'));
      },
    );
  }

  Widget _buildAlertResultsStream(LocationQueryBLoC bloc) {
    return StreamBuilder<Map<String, String>>(
      stream: bloc.alertResultsStream$,
      builder: (context, snapshot) {
        return Center(child: Text('%: ${snapshot.data}'));
      },
    );
  }

  Widget _actionUploadFile(LocationQueryBLoC bloc) {
    return IconButton(
        icon: Icon(Icons.cloud_upload), onPressed: bloc.upLoadFile);
  }

  Widget _actionCheckBattery(LocationQueryBLoC bloc) {
    return IconButton(
        icon: Icon(Icons.battery_std), onPressed: bloc.checkBattery);
  }

  Widget _buildEmailStream(LocationQueryBLoC bloc) {
    return StreamBuilder<bool>(
      stream: bloc.handelRegister$,
      builder: (context, snapshot) {
        return Center(child: Text('handelRegister: ${snapshot.data}'));
      },
    );
  }
}
