import 'package:flutter/material.dart';
import 'package:superteam/Repo/Restaurant.dart';
import 'package:superteam/Screens/Detail_restaurant_screen.dart';

class RestaurantContainer extends StatelessWidget {
  final Restaurant restaurant;
  const RestaurantContainer({
    Key key,
    @required this.restaurant,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListTile(
      leading: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            color: Colors.deepOrange,
            image: restaurant.imageUrl.contains("http")
                ? DecorationImage(image: NetworkImage(restaurant.imageUrl))
                : null),
      ),
      title: Text(restaurant.name),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                DetailRestaurantScreen(restaurant),
          ),
        );
      },
    );
  }


}