import 'package:flutter/material.dart';
import 'package:superteam/Screens/Home_screen.dart';
import 'package:superteam/Screens/Login_screen.dart';
import 'Location_screen.dart';
import 'package:superteam/BLoC/bloc_provider.dart';

import 'package:superteam/BLoC/login_bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return StreamBuilder<bool>(
      initialData: false,
      stream: BlocProvider.of<LoginBLoC>(context).connectionStream$,
      /// From that moment on, any widget part of the sub-tree, starting at BlocProvider will be able to get access to the IncrementBloc, via the following line:
      builder: (context, snapshot) {
        final connectionted = snapshot.data;
        print('Connectionted: $snapshot');
        if (!connectionted) {
          return LoginScreen();
        }
        return HomeScreen();
      },
    );
  }
}
