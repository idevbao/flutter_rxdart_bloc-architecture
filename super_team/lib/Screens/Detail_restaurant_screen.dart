import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'package:superteam/BLoC/favorite_bloc.dart';
import 'package:superteam/Repo/Restaurant.dart';

class DetailRestaurantScreen extends StatelessWidget {
  final Restaurant restaurant;
  DetailRestaurantScreen(this.restaurant);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = BlocProvider.of<FavoriteBloc>(context);
    return Scaffold(
      appBar: AppBar(title: Text(restaurant.name)),
      body: _buildDetail(bloc, context),
    );
  }

  Widget _buildDetail(FavoriteBloc bloc, BuildContext context) {
    return StreamBuilder<List<Restaurant>>(
      stream: bloc.favoriteStream,
      initialData: bloc.favorites,
      builder: (context, snapshot) {
        List<Restaurant> favorites =
            (snapshot.connectionState == ConnectionState.waiting)
                ? bloc.favorites
                : snapshot.data;
        bool isFavorite = favorites.contains(restaurant);
        return Center(
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
              color: Colors.blue,
              image: restaurant.imageUrl.contains("http")
                  ? DecorationImage(image: NetworkImage(restaurant.imageUrl))
                  : null,
            ),
            child: IconButton(
              color: Colors.green,
                icon: isFavorite
                    ? Icon(Icons.favorite)
                    : Icon(Icons.favorite_border),
                onPressed: () {
                  return bloc.tapRestaurant(restaurant);
                }),
          ),
        );
      },
    );
  }
}
