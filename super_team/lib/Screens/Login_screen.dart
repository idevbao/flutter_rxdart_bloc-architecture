import 'package:flutter/material.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'package:superteam/BLoC/login_bloc.dart';
import 'package:superteam/Common/Validator.dart';
import 'package:superteam/Screens/Home_screen.dart';


class LoginScreen extends StatelessWidget {
  final bloc = LoginBLoC();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    bloc.loginStream$.listen((onData) {
      onData ? Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      ) : print('object');
    });
    return BlocProvider<LoginBLoC>(
        bloc: bloc,
        child: Scaffold(
          body: _widgetBody(bloc, context),
          resizeToAvoidBottomInset: false,
        ));
  }

  Widget _widgetBody(LoginBLoC bloc, BuildContext context) {
    final heightScreen =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    var paddingStatusBar =
        EdgeInsets.fromLTRB(0.0, MediaQuery.of(context).padding.top, 0.0, 0.0);
    return Container(
        padding: paddingStatusBar,
        color: Colors.white,
        transform: MediaQuery.of(context).viewInsets.bottom > 0.0
            ? Matrix4.translationValues(
                0.0, -(MediaQuery.of(context).viewInsets.bottom), 0.0)
            : Matrix4.translationValues(0.0, 0.0, 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _widgetLogo(context, heightScreen),
            _widgetSlogan(context, heightScreen),
            _widgetLogin(context, heightScreen),
            _widgetOther(context, heightScreen)
          ],
        ));
  }

  Widget _widgetLogo(context, heightS) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: heightS * 0.2,
      child: Text(
        '<Super dev>',
        style: TextStyle(
          backgroundColor: Colors.white,
          color: Colors.blue,
          fontSize: 40.0,
          fontStyle: FontStyle.italic,
//          fontWeight: FontWeight.bold
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _widgetSlogan(context, heightS) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: heightS * 0.25,
      child: PageView(
        children: <Widget>[
          Container(
            color: Colors.lightGreen,
            child: Text(
              'Focus on the point',
              style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 20.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            child: Text(
              'Focus on the point2',
              style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 20.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }

  Widget _widgetLogin(context, heightS) {
    /// Config View login
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    var isAlignment = MediaQuery.of(context).viewInsets.bottom > 0.0
        ? MainAxisAlignment.center
        : MainAxisAlignment.center;
    var isMainAxisSize = MainAxisSize.min;
    var widthIDLabel = MediaQuery.of(context).size.width * 0.2;
    var widthLoginContainer = MediaQuery.of(context).size.width * 0.85;
    var heightLoginContainer = isPortrait ? heightS * 0.25 : heightS * 0.25;

    return Container(
      width: widthLoginContainer,
      height: heightLoginContainer,
      child: Column(
        mainAxisSize: isMainAxisSize,
        mainAxisAlignment: isAlignment,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                border: Border.all(
                    color: Colors.lightBlueAccent, style: BorderStyle.solid)),
            child: Row(
              children: <Widget>[
                Container(
                  width: widthIDLabel,
                  child: Text("Apple ID"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: StreamBuilder<Set<ValidationError>>(
                      stream: bloc.emailError$,
                      builder: (context, snapshot) {
                        return TextField(
                          decoration: InputDecoration(
                              errorText: _getMessage(snapshot.data),
                              border: InputBorder.none,
                              hintText: 'Required'),
                          onChanged: bloc.inputID,
                        );
                      }),
                )
              ],
            ),
          ),
          Container(
            transform: Matrix4.translationValues(0.0, -5.0, 0.0),
            padding: const EdgeInsets.fromLTRB(16.0, 0, 16.0, 2.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0)),
                border: Border.all(
                    color: Colors.lightBlueAccent, style: BorderStyle.solid)),
            child: Row(
              children: <Widget>[
                Container(
                  width: widthIDLabel,
                  child: Text("Password"),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: StreamBuilder<Set<ValidationError>>(
                      stream: bloc.passwordError$,
                      builder: (context, snapshot) {
                        return TextField(
                          obscureText: true,
                          decoration: InputDecoration(
                              errorText: _getMessage(snapshot.data),
                              border: InputBorder.none,
                              hintText: 'Required'),
                          onChanged: bloc.inputPassword,
                        );
                      }),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _widgetOther(context, heightS) {
    /// Config View login
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    var isAlignment = MediaQuery.of(context).viewInsets.bottom > 0.0
        ? MainAxisAlignment.start
        : MainAxisAlignment.start;
    var isMainAxisSize = MainAxisSize.min;
    var widthOtherContainer = MediaQuery.of(context).size.width * 0.85;
    var heightLoginContainer = isPortrait ? heightS * 0.3 : heightS * 0.01;

    return Container(
      width: widthOtherContainer,
      height: heightLoginContainer,
      child: Column(
        mainAxisSize: isMainAxisSize,
        mainAxisAlignment: isAlignment,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(),
            child: StreamBuilder<bool>(
                initialData: false,
                stream: bloc.loginStream$,
                builder: (context, snapShot) {
                  return FlatButton(
                      onPressed: bloc.loginAction,
                      child: Text(
                        'Sign In..',
                        style: TextStyle(
                          color:  Colors.lightBlueAccent,
                          fontSize: 20.0,
//              fontStyle: FontStyle.italic,
                        ),
                      ));
                }),
          ),
          Container(
            decoration: BoxDecoration(),
            child: FlatButton(
                onPressed: () =>
                    {print('Forgot Apple ID or Password? onPressed')},
                child: Text(
                  'Forgot Apple ID or Password?',
                  style: TextStyle(
                    color: Colors.lightBlueAccent,
//                    fontSize: 20.0,
//              fontStyle: FontStyle.italic,
                  ),
                )),
          ),
          Container(
            decoration: BoxDecoration(),
            child: FlatButton(
                onPressed: () => {print('Setup instructions onPressed')},
                child: Text(
                  'Setup instructions',
                  style: TextStyle(
                    color: Colors.lightBlueAccent,
//                    fontSize: 20.0,
//              fontStyle: FontStyle.italic,
                  ),
                )),
          )
        ],
      ),
    );
  }


  String _getMessage(Set<ValidationError> errors) {
    if (errors == null || errors.isEmpty) {
      return null;
    }
    if (errors.contains(ValidationError.invalidEmail)) {
      return 'Invalid email address';
    }
    if (errors.contains(ValidationError.tooShortPassword)) {
      return 'Password must be at least 6 characters';
    }
    return null;
  }
}
