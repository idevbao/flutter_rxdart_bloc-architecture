import 'package:flutter/material.dart';
import 'package:superteam/BLoC/restaurant_bloc.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'Restaurant_container.dart';
import 'package:superteam/Repo/Restaurant.dart';
import 'package:superteam/Repo/Location.dart';

class RestaurantScreen extends StatelessWidget {
  final Location location;

  const RestaurantScreen({Key key, @required this.location}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final bloc = RestaurantBLoC(location);
    return BlocProvider(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(title: Text('Where do you want to eat?')),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 10.0),
              child: TextField(
                onChanged: (query) {
                  bloc.submitQuery(query);
                },
              ),
            ),
            Expanded(
              child: _buildResults(bloc),
            )
          ],
        ),
      ),
    );
  }
}

Widget _buildResults(RestaurantBLoC bloc) {
  return StreamBuilder<List<Restaurant>>(
    stream: bloc.stream,
    builder: (context, snapshot) {
      final results = snapshot.data;
      if (results == null) {
        return Center(child: Text('Enter a restaurant'));
      }
      if (results.isEmpty) {
        return Center(child: Text('No Results'));
      }
      return _buildSearchResults(results);
    },
  );
}

Widget _buildSearchResults(List<Restaurant> results) {
  return ListView.separated(
    itemCount: results.length,
    separatorBuilder: (BuildContext context, int index) => Divider(),
    itemBuilder: (context, index) {
      final restaurant = results[index];
      return RestaurantContainer(restaurant: restaurant);
    },
  );
}
