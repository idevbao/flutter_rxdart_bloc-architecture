import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'package:superteam/BLoC/home_bloc.dart';
//import 'package:superteam/Repo/
import 'dart:io';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = HomeBLoC();
    final widthScreen = MediaQuery.of(context).size.width;
    final heightScreen =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // TODO: implement build
    return BlocProvider<HomeBLoC>(
      bloc: bloc,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Container(
            width: MediaQuery.of(context).size.width,
            child: TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black26,
                  ),
                  border: InputBorder.none,
                  hintText: 'Search by brand, color, etc'),
              onChanged: bloc.inputID,
            ),
          ),
          actions: <Widget>[],
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.green,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          currentIndex: 0,
          items: [
            BottomNavigationBarItem(
                icon: new Icon(Icons.attach_money), title: Text('')),
            BottomNavigationBarItem(
                icon: new Icon(Icons.youtube_searched_for), title: Text('')),
            BottomNavigationBarItem(
                icon: new Icon(Icons.account_circle), title: Text(''))
          ],
        ),
        body: DefaultTabController(
          length: 6,
          child: Column(children: <Widget>[
            TabBar(
                  isScrollable: true,
                tabs: _tabScroll(widthScreen),
                onTap: (index) {},
                labelColor: Colors.black87,
                unselectedLabelColor: Colors.grey,
                indicatorColor: Colors.green

            ),

            Expanded(
              child: Container(
                child: TabBarView(children: [
                  Container(
                    child: Text("Most Popular SNEAKER"),
                  ),
                  Container(
                    child: Text("Most Popular STREETWEAR"),
                  ),
                  Container(
                    child: Text("Most Popular COLLECTIBLES"),
                  ),
                  Container(
                    child: Text("Most Popular WATCHES"),
                  ),
                  Container(
                    child: Text("Most Popular HANDBAGS"),
                  ),
                  Container(
                    child: Text("Most Popular SOCKS"),
                  ),
                ]),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  List<Widget> _tabScroll(widthScreen) => [
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.18),
            child: Text("SNEAKER"),
          ),
        ),
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.25),
            child: Text("STREETWEAR"),
          ),
        ),
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.25),
            child: Text("COLLECTIBLES"),
          ),
        ),
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.25),
            child: Text("WATCHES"),
          ),
        ),
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.25),
            child: Text("HANDBAGS"),
          ),
        ),
        Tab(
          child: Container(
            alignment: Alignment.center,
            constraints: BoxConstraints.expand(width: widthScreen * 0.25),
            child: Text("SOCKS"),
          ),
        ),
      ];
}
