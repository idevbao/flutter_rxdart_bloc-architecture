import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:superteam/BLoC/favorite_bloc.dart';
import 'package:superteam/Screens/Main_screen.dart';
import 'package:superteam/BLoC/bloc_provider.dart';
import 'package:superteam/BLoC/location_bloc.dart';
import 'package:superteam/BLoC/login_bloc.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBLoC>(
      bloc: LoginBLoC(),
      child: MaterialApp(
        title: 'Restaurant Finder',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        home: MainScreen(),
      ),
    );
  }
}
