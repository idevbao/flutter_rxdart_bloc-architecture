import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

class HandleAut {
  Future<Map<String, String>> verifyPhoneNumber(String _phoneNumber) async {
    print(_phoneNumber);
    var verificationIdResult = '';
    var alertResults = '';
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final PhoneVerificationCompleted verificationCompleted =
        (AuthCredential phoneAuthCredential) {
      _auth.signInWithCredential(phoneAuthCredential);
      print('Received phone auth credential: $phoneAuthCredential');
      return alertResults =
          'Received phone auth credential: $phoneAuthCredential';
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      print(
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
      return alertResults =
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}';
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      print('Please check your phone for the verification code. $verificationId');
      verificationIdResult = verificationId;

      return {'verificationIdResult':verificationIdResult,'alertResults': alertResults};
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      verificationIdResult = verificationId;

      return {'verificationIdResult': verificationIdResult, 'alertResults': alertResults};
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: _phoneNumber,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    return {'verificationIdResult': verificationIdResult,'alertResults': alertResults};
  }

  // Example code of how to sign in with phone.
  Future<bool> signInWithPhoneNumber(_verificationId, _smsCode) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: _verificationId,
      smsCode: _smsCode,
    );
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    if (user != null) {
      print('Successfully signed in, uid: ' + user.uid);
      return true;
    } else {
      print('Sign in failed');
      return false;
    }
  }

  Future<bool> signInWithEmailAndPassword(_email, _password) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
      email: _email,
      password: _password,
    )).user;
    print('signInWithEmailAndPassword');
    if (user != null) {
      print("success 0k $user ", );
      return true;
    } else {
      print("fail");
      return false;
    }
  }


  Future<bool> register(String _email,String _password) async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
      email: _email,
      password: _password,
    )).user;
    if (user != null) {
      print('register ok $user');
      return true;
    } else {
      print('register false');
      return false;
    }
  }

  Stream<bool> checkLogin() {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseUser user;
    _auth.onAuthStateChanged.listen((onData) => ( user = onData));
    if (user != null) {
      print('checkLogin ok $user');
      return Stream.value(true);
    } else {
      print('checkLogin false  $user');
      return Stream.value(false);
    }

  }
}
