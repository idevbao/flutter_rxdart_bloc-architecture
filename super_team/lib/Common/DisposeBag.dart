import 'dart:async';


class DisposeBag {
  final _resources = <dynamic>{}; // <StreamSubscription | Sink>{}

  DisposeBag([Iterable<dynamic> disposables = const []]) {
    _addAll(disposables);
  }

  void _addAll(Iterable<dynamic> disposables) {
    for (final item in disposables) {
      _addOne(item);
    }
  }

  /// Add one item to resouces, only add if item is [StreamSubscription] or item is [Sink]
  bool _addOne(dynamic item) {
    if (item == null) {
      return false;
    }
    if (item is StreamSubscription || item is Sink) {
      return _resources.add(item);
    }
    return false;
  }


  /// Cancel [StreamSubscription] or close [Sink]
  ///
  Future<dynamic> _disposeOne(dynamic disposable) {
    if (disposable is StreamSubscription) {
      print('cancel StreamSubscription $disposable');
      return disposable.cancel();
    }
    if (disposable is StreamSink) {
      print('close StreamSink $disposable');
      return disposable.close();
    }
    if (disposable is Sink) {
      print('close Sink $disposable');
      disposable.close();
      return null;
    }
    return null;
  }

  Future<void> _clear() async {

    /// Start dispose


    /// Await dispose
    final futures =
    Set.of(_resources).map(_disposeOne).where((future) => future != null);
    await Future.wait(futures);

    /// End dispose

    _resources.clear();
  }



  Future<void> clear() => _clear();

  Future<void> dispose() async {
    await _clear();
  }
}

