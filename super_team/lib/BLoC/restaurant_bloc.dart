import 'dart:async';
import 'Base/bloc.dart';
import 'package:superteam/Repo/Location.dart';
import 'package:superteam/Repo/Restaurant.dart';
import 'package:superteam/Domain/Api.dart';

class RestaurantBLoC implements Bloc {
  final Location location;
  final _client = ZomatoClient();
  final _controller = StreamController<List<Restaurant>>();

  Stream<List<Restaurant>> get stream => _controller.stream;
  RestaurantBLoC(this.location);

  void submitQuery(String query) async {
    final results = await _client.fetchRestaurants(location, query);
    _controller.sink.add(results);
  }

  @override
  void dispose() {
    print('dispose RestaurantBLoC');
    _controller.close();
  }
}
