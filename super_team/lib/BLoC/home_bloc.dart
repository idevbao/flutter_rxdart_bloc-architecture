import 'dart:async';
import 'dart:ffi';

import 'package:superteam/Channel/battery.dart';
import 'package:superteam/Common/DisposeBag.dart';
import 'Base/bloc.dart';
import 'package:superteam/Repo/Location.dart';
import 'package:superteam/Domain/Api.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';

import 'package:superteam/Firebase/HandelAuth.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomeBLoC implements Bloc {
  /// Input
  final void Function() loginAction;
  final void Function(double) heightScreen;

  final void Function(String) inputID;
  final void Function(String) inputPassword;

  /// Bag
  final void Function() disposeLogin;

  /// OutPut
  final Stream<String> infoUserStream$;
  final Stream<FirebaseUser> connectionStream$;
  final Stream<double> heightScreenStream$;
  final Stream<bool> loginStream$;

  HomeBLoC._(
      {@required this.loginAction,
        @required this.loginStream$,
        @required this.inputID,
        @required this.inputPassword,
        @required this.infoUserStream$,
        @required this.connectionStream$,
        @required this.disposeLogin,
        @required this.heightScreen,
        @required this.heightScreenStream$});

  factory HomeBLoC() {
    final handle = HandleAut();

    /// Handle height Screen
    // ignore: close_sinks
    final inputHeightScreen = PublishSubject<double>();

    final heightScreenStream$ =
    inputHeightScreen.distinct().exhaustMap((mapper) {
      return Stream.value(mapper);
    });

    /// Handle ID PW -> Login
    // ignore: close_sinks
    final id = PublishSubject<String>();
    // ignore: close_sinks
    final pw = PublishSubject<String>();
    // ignore: close_sinks
    final handleLogin = PublishSubject<Void>();

    final id$ =
    id.debounceTime(const Duration(milliseconds: 900)).
    exhaustMap((mapper) {
      print('mapper id $mapper');
      return Stream.value(mapper);
    });

    final pw$ =
    pw.debounceTime(const Duration(milliseconds: 900)).exhaustMap((mapper) {
      print('mapper pw $mapper');
      return Stream.value(mapper);
    });

    var data = Rx.combineLatest2(id$, pw$, (id, pass) {
      print('combinelastest $id , $pass');
      return [id, pass];
    });

    final _handelLogin = handleLogin
        .throttleTime(const Duration(milliseconds: 100)).withLatestFrom((data), (_ , data) {
      return data;
    }).asyncExpand((data) {
      return Stream.fromFuture(handle.signInWithEmailAndPassword(data[0], data[1]));
    });

    /// connection

    final connectionStream$ =
    Stream.fromFuture(FirebaseAuth.instance.currentUser());

    /// Bag
    final bag = [
      connectionStream$,
      inputHeightScreen,
      heightScreenStream$,
      handleLogin,
      _handelLogin,
      id,
      pw,
      id$,
      pw$
    ];

    return HomeBLoC._(
        disposeLogin: DisposeBag([...bag]).dispose,
        inputID: id.add,
        inputPassword: pw.add,
        loginAction: () => handleLogin.add(null),
        connectionStream$: connectionStream$,
        heightScreen: inputHeightScreen.add,
        heightScreenStream$: heightScreenStream$,
        loginStream$: _handelLogin);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose LocationQueryBLoC');
    return disposeLogin();
  }
}
