import 'Base/bloc.dart';
import 'dart:async';
import 'package:superteam/BLoC/Base/bloc.dart';
import 'package:superteam/Repo/Location.dart';

class LocationBLoC implements Bloc {
  /// private
  Location _location;
  final _locationController = StreamController<Location>();

  /// public get
  Location get selectedLocation => _location;

  /// Here a private StreamController is declared that will manage the stream and sink for this BLoC.
  ///
  /// Stream<Location> type system what kind of object will be emitted from the stream.
  Stream<Location> get locationStream => _locationController.stream;

  void selectLocation(Location location) {
    _location = location;
    _locationController.sink.add(location);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose LocationBLoC');
    _locationController.close();
  }
}
