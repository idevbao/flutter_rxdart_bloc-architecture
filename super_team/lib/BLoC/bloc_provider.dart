import 'package:flutter/cupertino.dart';
import 'package:superteam/BLoC/Base/bloc.dart';

/// BlocProvider is a generic class. The generic type T is scoped to be an object that implements the Bloc interface.
/// This means that the provider can only store BLoC objects
/// Different bloc need implements bloc
/// It using in a Screen with mission build sate and manager there widget
class BlocProvider<T extends Bloc> extends StatefulWidget {
  final Widget child;
  final T bloc;

  /// call den cac bloc dc quan ly

  /// new
  const BlocProvider({Key key, @required this.bloc, @required this.child})
      : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return null;
//  }

  /// The of method allows widgets to retrieve the BlocProvider from a descendant in the widget tree with the current build context.
  /// This is a very common pattern in Flutter.
  /// The of widget child call the widget current

  static Type _typeOf<T>() => T;

  static T of<T extends Bloc>(BuildContext context) {
    // TODO: implement build
    final BlocProvider<T> provider =
        context.findAncestorWidgetOfExactType<BlocProvider<T>>();
    if (provider == null) {
      throw StateError(
          'Cannot get provider with type ${_typeOf<BlocProvider<T>>()}');
    }
    print(provider.bloc);
    return provider.bloc;
  }

  @override
  State createState() => _BlocProviderState();
}

class _BlocProviderState extends State<BlocProvider> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return widget.child;

    ///   T get widget => _widget;
    ///  T _widget;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose FavoriteBloc');
    widget.bloc.dispose();
    super.dispose();
  }
}
