import 'dart:async';
import 'dart:ffi';
import 'package:path_provider/path_provider.dart';
import 'package:superteam/Channel/battery.dart';
import 'package:superteam/Common/DisposeBag.dart';
import 'Base/bloc.dart';
import 'package:superteam/Repo/Location.dart';
import 'package:superteam/Domain/Api.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';
import 'dart:io';
import 'package:superteam/Firebase/HandelAuth.dart';

class LocationQueryBLoC implements Bloc {
  /// Input
  final void Function(String) changeQuery;
  final void Function() upLoadFile;
  final void Function() checkBattery;
  final void Function(String) phoneText;
  final void Function(String) email;
  final void Function(String) pass;
  final void Function() login;

  /// OutPut
  final void Function() disposeClear;

  final Stream<List<Location>> locationStream$;
  final Stream<Directory> getLibraryDirectory$;
  final Stream<String> getBatteryStream$;
  final Stream<Map<String, String>> alertResultsStream$;
  final Stream<bool> handelRegister$;

  LocationQueryBLoC._(
      {@required this.changeQuery,
      @required this.locationStream$,
      @required this.disposeClear,
      @required this.upLoadFile,
      @required this.getLibraryDirectory$,
      @required this.checkBattery,
      @required this.getBatteryStream$,
      @required this.phoneText,
      @required this.alertResultsStream$,
      @required this.handelRegister$,
      @required this.email,
      @required this.pass,
      @required this.login});

  factory LocationQueryBLoC() {
    final _client = ZomatoClient();
    final _channel = Battery();

    /// Handle Search
    // ignore: close_sinks
    final _onTextChanged = PublishSubject<String>();

    final locationStream$ = _onTextChanged
        .debounceTime(const Duration(milliseconds: 1000))
        .asyncExpand(
            (value) => Stream.fromFuture(_client.fetchLocations(value)));
    // ignore: close_sinks
    final _onClickGetBatteryChanged = PublishSubject<Void>();

    final batteryStream$ = _onClickGetBatteryChanged
        .throttleTime(const Duration(milliseconds: 1000))
        .asyncExpand((_) => Stream.fromFuture(_channel.getBatteryLevel()));

    /// Handle Upload
    ///
    // ignore: close_sinks
    final _onTap = PublishSubject<Void>();
    // ignore: close_sinks
    final _getLibraryDirectory$ = StreamController<Directory>();

    _onTap.throttleTime(const Duration(milliseconds: 6000)).listen((onData) {
      _getLibraryDirectory$.addStream(_getLibraryDirectory());
    });
    // ignore: close_sinks
    final phoneText = PublishSubject<String>();
    final handle = HandleAut();
    final alertResultsStream$ = phoneText
        .debounceTime(const Duration(milliseconds: 5000))
        .asyncExpand((value) {
      return Stream.fromFuture(handle.verifyPhoneNumber(value));
    });
    // ignore: close_sinks
    final email = PublishSubject<String>();
    // ignore: close_sinks
    final password = PublishSubject<String>();
//
    var aaa =  '';

    final valueEmail =
        email.debounceTime(const Duration(milliseconds: 30)).listen((onData){
          aaa = onData;
        });

    var bbb = "";
  final valuePassword = password
        .debounceTime(const Duration(milliseconds: 30)
    ).listen((onData){
    bbb = onData;
    });
    // ignore: close_sinks
    final register = PublishSubject<Void>();

    final handelRegister = register
        .throttleTime(const Duration(milliseconds: 300))
        .asyncExpand((convert) {
      var values = Rx.combineLatest2(Stream.value(aaa), Stream.value(bbb), (email, pass) {
        print('combinelastest $email , $pass');
        return [email, pass];
      });
      return values.asyncExpand((convert) {
        return Stream.fromFuture(handle.register(convert[0], convert[1]));
      });

    });

    /// Bag
    final bag = [
      phoneText,
      alertResultsStream$,
      _onTap,
      _getLibraryDirectory$,
      _onTextChanged,
      locationStream$,
      _onClickGetBatteryChanged,
      batteryStream$,
      register,
      email,
      password,
      valueEmail,
      valuePassword
    ];

    return LocationQueryBLoC._(
        disposeClear: DisposeBag([...bag]).dispose,
        /// Handle Locations
        changeQuery: _onTextChanged.add,
        locationStream$: locationStream$,

        /// Handle Battery
        checkBattery: () => _onClickGetBatteryChanged.add(null),
        getBatteryStream$: batteryStream$,
        upLoadFile: () => _onTap.add(null),
        getLibraryDirectory$: _getLibraryDirectory$.stream,
        phoneText: phoneText.add,
//        smsCode: locationStream$,
        //        toHomeStream$: toHomeStream$,
        email: email.add,
        pass: password.add,
        login: () => register.add(null),
        alertResultsStream$: alertResultsStream$,
        handelRegister$: handelRegister);
  }

  static Stream<Directory> _getLibraryDirectory() async* {
    try {
      final result = await getTemporaryDirectory();
      yield result;
    } catch (e) {
      yield null;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose LocationQueryBLoC');
    return disposeClear();
  }
}
