import 'dart:async';
import 'package:superteam/BLoC/Base/bloc.dart';
import 'package:superteam/Repo/Restaurant.dart';

class FavoriteBloc implements Bloc {
  var _restaurants = <Restaurant>[];
  final _controller = StreamController<List<Restaurant>>.broadcast();

  List<Restaurant> get favorites => _restaurants;
  Stream<List<Restaurant>> get favoriteStream => _controller.stream;

  void tapRestaurant(Restaurant restaurant) {

    _restaurants.contains(restaurant) ? _restaurants.remove(restaurant) : _restaurants.add(restaurant);
    print(restaurant.name );
    print( _restaurants.length);
    _controller.sink.add(_restaurants);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose FavoriteBloc');
    _controller.close();
  }
}
