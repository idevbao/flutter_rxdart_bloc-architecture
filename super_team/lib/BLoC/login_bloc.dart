import 'dart:async';
import 'package:superteam/Common/DisposeBag.dart';
import 'Base/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';
import 'package:superteam/Common/Validator.dart';
import 'package:superteam/Firebase/HandelAuth.dart';

class LoginBLoC implements Bloc {
  /// Input
  final void Function() loginAction;
  final void Function(String) inputID;
  final void Function(String) inputPassword;

  /// Bag
  final void Function() disposeLogin;

  /// OutPut
  final Stream<String> infoUserStream$;
  final Stream<bool> connectionStream$;
  final Stream<bool> loginStream$;
  final Stream<Set<ValidationError>> emailError$;
  final Stream<Set<ValidationError>> passwordError$;
  final Stream<bool> checkValidate$;

  LoginBLoC._(
      {@required this.loginAction,
      @required this.loginStream$,
      @required this.inputID,
      @required this.inputPassword,
      @required this.infoUserStream$,
      @required this.connectionStream$,
      @required this.disposeLogin,
      @required this.emailError$,
      @required this.passwordError$,
      @required this.checkValidate$});

  factory LoginBLoC() {
    final handle = HandleAut();
    const validator = Validator();

    /// Handle ID PW -> Login
    // ignore: close_sinks
    final id = BehaviorSubject.seeded('');
    // ignore: close_sinks
    final pw = BehaviorSubject.seeded('');

    // ignore: close_sinks
    final login = PublishSubject();

    final idError$ = id.debounceTime(const Duration(milliseconds: 500)).map(validator.validateEmail);

    final passwordError$ = pw.debounceTime(const Duration(milliseconds: 500)).map(validator.validatePassword);

    final checkValidate$ =
        Rx.combineLatest([idError$, passwordError$], (listOfSets) {
      return listOfSets.every((errorsSet) => errorsSet.isEmpty);
    }).asBroadcastStream();

    final handelLogin = login
        .withLatestFrom(checkValidate$, (_, isValid) {
          return isValid;
        })
        .where((isValid) => isValid)
        .withLatestFrom2(
            id.debounceTime(const Duration(milliseconds: 500)),
            pw.debounceTime(const Duration(milliseconds: 500)),
            (_, id, password) => [id, password])
        .asyncExpand((obj) {
          print(obj);
          return Stream.fromFuture(
              handle.signInWithEmailAndPassword(obj[0], obj[1]));
        });

    /// connection

    final connectionStream$ = handle.checkLogin();

    /// Bag
    final bag = [
      login,
      id,
      pw,
      handelLogin,
    ];

    return LoginBLoC._(
      disposeLogin: DisposeBag([...bag]).dispose,
      inputID: id.add,
      inputPassword: pw.add,
      loginAction: () => login.add(null),
      connectionStream$: connectionStream$,
      loginStream$: handelLogin,
      emailError$: idError$,
      passwordError$: passwordError$,
      checkValidate$: checkValidate$,
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print('dispose LocationQueryBLoC');
    this.disposeLogin();
  }
}
